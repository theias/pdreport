<?php

namespace Ias\ProjectDependencyReport\Command;

use SensioLabs\Security\Exception\RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use SensioLabs\Security\SecurityChecker;

class ReportCommand extends Command
{
    /**
     * @var array
     */
    private $config;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct('pdreport:report');
        $this->config = $config;
    }

    protected function configure()
    {
        $this
            ->setName('pdreport:report')
            ->setDescription('Analyze and report on multiple composer lock files');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $parsed = $this->parseLocks();
        if (isset($parsed['ignore'])) {
            foreach ($parsed['ignore'] as $app) {
                $output->writeln("<question>No lock file found for '$app' in directory '{$this->config['projects'][$app]}': project ignored.</question>");
                unset($this->config['projects'][$app]);
           }
           unset($parsed['ignore']);
        }
        $headers = array_merge(['<comment>Package</comment>'], array_keys($this->config['projects']));
        $packages = $this->packageList($parsed);
        $problems = $this->findProblems($this->config['projects'], $headers);
        $headers = $this->annotateHeaders($headers, $problems);
        $table = new Table($output);
        $table
            ->setHeaders($headers)
            ->setRows($this->generateTable($packages, $parsed, $problems))
            ->render();
    }

    private function parseLocks() {
        $result = [];
        foreach ($this->config['projects'] as $app => $lock) {
            $lockfile = $lock . "/composer.lock";
            if (is_readable($lockfile)) {
                $headers[] = $app;
                $file = file_get_contents($lockfile);
                $parse = json_decode($file, true);
                foreach ($parse['packages'] as $package) {
                    $result[$app][$package['name']] = $package['version'];
                }
            } else {
                $result['ignore'][] = $app;
            }
        }
        return $result;
    }

    private function packageList($array) {
        $packageArray = [];
        foreach ($array as $app => $packages) {
            $packageArray = array_merge($packageArray, array_keys($packages));
        }
        sort($packageArray);
        return array_unique($packageArray);
    }

    private function findProblems($projects, $headers) {
        $problems = [];
        $checker = new SecurityChecker();
        for ($i=1; $i<count($headers); $i++) {
            $securityJson = $checker->check($projects[$headers[$i]]);
            if (count($securityJson) != 0) {
                $problems[$headers[$i]] = array_keys($securityJson);
            }
        }
        return $problems;
    }

    private function generateTable($packages, $parsed, $problems) {
        foreach ($packages as $package) {
            $row[0] = $package;
            $i=1;
            foreach ($parsed as $app => $installed) {
                if (isset($installed[$package])) {
                    $row[$i] = $installed[$package];
                    if (isset($problems[$app]) && in_array($package, $problems[$app])) {
                        $row[$i] = $this->markError($row[$i]);
                    }
                } else {
                    $row[$i] = '';
                }
                $i++;
            }
            $report[] = $row;
            unset($row);
        }
        return $report;
    }

    private function annotateHeaders($headers, $problems) {
        for ($i=1; $i<count($headers); $i++) {
            if (isset($problems[$headers[$i]])) {
                $headers[$i] = $this->markError($headers[$i]);
            }
        }
        return $headers;
    }

    private function markError($string) {
        $styling = isset($this->config['errorStyling']) ? $this->config['errorStyling'] : ["<error>", "</error>"];
        return $styling[0] . $string . $styling[1];
    }
}


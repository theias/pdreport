# Composer Projects Dependencies Report

![Example image](doc/example.png "Example image")

This console command analyzes [Composer](https://getcomposer.org/) lock files
in __multiple projects__, presents a report comparing dependency versions and
also employs the SensioLabs Security Checker to mark 
 dependencies with known security vulnerabilities
 
 ![Example image](doc/example2.png "Example image")

## Installation

  Install [Composer](https://getcomposer.org/) if you have not already got it.

    composer install
    cp pdreport.json.dist pdreport.json

## Usage

    bin/pdreport
    
## Configuration

Edit pdreport.json. Modify the list of composer-based projects and their filesystem
paths.

    {
        "projects": {
            "PDReport": "./",
            "Foo": "/var/www/html/foo"
        },
        "errorStyling": ["<error>", "</error>"]
    }
    
Optionally, edit the errorStyling.

## Credits

  * Edna Wigderson: original idea
  * Chris McCafferty: helped

## License

MIT
